﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public Animator header;
    public Animator startButton;
    public Animator settingsButton;
    public Animator Settingsdialog;
    public Animator SlideButton;
    public Animator contentPanel;
    public GameObject Audiopanel;
    public Text settingsLabel;
    public Text shareText;
    public GameObject[] Settingsdialogs;
    public GameObject PLight;
    public GameObject PDark;
    public GameObject NLight;
    public GameObject NDark;

    private int currentPage = 0;


    public void Update()
    {
        switch (currentPage)
        {
            case 0:
                settingsLabel.text = "Audio";
                break;
            case 1:
                settingsLabel.text = "Graphic";
                break;
            case 2:
                settingsLabel.text = "Control";
                break;
        }
    }

    public void StartGame()
    {
        header.SetBool("isHidden", true);
        startButton.SetBool("isHidden", true);
        settingsButton.SetBool("isHidden", true);
        SlideButton.SetBool("isHidden", true);
        Audiopanel.SetActive(false);
    }

    public void OpenSettings()
    {
        header.SetBool("isHidden", true);
        startButton.SetBool("isHidden", true);
        settingsButton.SetBool("isHidden", true);
        Settingsdialog.SetBool("isHidden", false);
    }

    public void CloseSettings()
    {
        header.SetBool("isHidden", false);
        startButton.SetBool("isHidden", false);
        settingsButton.SetBool("isHidden", false);
        Settingsdialog.SetBool("isHidden", true);
    }

    public void ToggleMenu()
    {
        bool isHidden = contentPanel.GetBool("isHidden");
        contentPanel.SetBool("isHidden", !isHidden);
    }

    public void NextPage()
    {
        Settingsdialogs[currentPage].SetActive(false);
        if (currentPage == 2)
        {
            currentPage = 0;
            Settingsdialogs[currentPage].SetActive(true);
        }
        else
        {
            currentPage += 1;
            Settingsdialogs[currentPage].SetActive(true);
        }
    }

    public void PreiousPage()
    {
        Settingsdialogs[currentPage].SetActive(false);
        if (currentPage == 0)
        {
            currentPage = 2;
            Settingsdialogs[currentPage].SetActive(true);
        }
        else
        {
            currentPage -= 1;
            Settingsdialogs[currentPage].SetActive(true);
        }
    }

    public void NEnter()
    {
        NDark.SetActive(false);
        NLight.SetActive(true);
    }

    public void NExit()
    {
        NLight.SetActive(false);
        NDark.SetActive(true);
    }

    public void PEnter()
    {
        PDark.SetActive(false);
        PLight.SetActive(true);
    }

    public void PExit()
    {
        PLight.SetActive(false);
        PDark.SetActive(true);
    }

    public void ShareTextEnter()
    {
        shareText.text = "Share your scores";
    }

    public void ShareTextExit()
    {
        shareText.text = "";
    }

    public void InsTextEnter()
    {
        shareText.text = "Share to Ins";
    }

    public void FBTextEnter()
    {
        shareText.text = "Share to Facebook";
    }

    public void MTextEnter()
    {
        shareText.text = "Share by message";
    }

}
